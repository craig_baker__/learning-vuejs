import Vue from "vue";
import App from "./App.vue";
import Vuex from "vuex";

Vue.config.productionTip = false;
Vue.use(Vuex);

export const FILTERS = {
  ALL: "ALL",
  COMPLETED: "COMPLETED",
  UNCOMPLETED: "UNCOMPLETED"
};

const store = new Vuex.Store({
  state: {
    todos: [
      { id: 1, done: false, text: "Buy milk" },
      { id: 2, done: false, text: "Buy pizza" },
      { id: 3, done: false, text: "Buy oranges" },
      { id: 4, done: true, text: "Buy couch" }
    ],
    filter: FILTERS.UNCOMPLETED
  },
  getters: {
    doneTodos: state => {
      return state.todos.filter(todo => todo.done);
    },
    uncompletedTodos: state => {
      return state.todos.filter(todo => !todo.done);
    }
  },
  mutations: {
    toggleTodo(state, id) {
      state.todos = state.todos.map(todo => {
        if (todo.id === id) {
          todo.done = !todo.done;
        }
        return todo;
      });
    },
    updateFilter(state, filter) {
      state.filter = filter;
    },
    addTodo(state, todoText) {
      state.todos = [
        ...state.todos,
        { id: state.todos.length + 1, text: todoText, done: false }
      ];
    }
  }
});

new Vue({
  render: h => h(App),
  store
}).$mount("#app");
